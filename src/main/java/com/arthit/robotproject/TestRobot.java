/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.robotproject;

/**
 *
 * @author Arthit
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot  = new Robot (20,50,99,99,100);
        System.out.println(robot);
        robot.walk('N');
        System.out.println(robot);
        robot.walk('S');
        robot.walk('S');
        System.out.println(robot);
        robot.walk('E', 2);
        System.out.println(robot);
        robot.walk();
        System.out.println(robot);
        robot.walk(6);
        System.out.println(robot);
        robot.walk('E',70);
        System.out.println(robot);
        robot.walk('S',48);
        System.out.println(robot);
        robot.walk('N',200);
        System.out.println(robot);
        
    }
}
