/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.robotproject;

/**
 *
 * @author Arthit
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char LastDirection = 'N';
    
    public Robot (int x , int y ,int bx , int by , int N){
        this.x = x;
        this.y = y;
        this.bx = bx;
        this.by = by;
        this.N = N;
    }
    
    public boolean inMap (int x , int y){
        if (x>=N || x<0 || y>=N || y<0){
            return false;
        }
        return true;
    }
    
    public boolean walk (char direction){
        switch (direction){
            case 'N':
                if(!inMap(x,y-1)){
                    printUnableMove();
                    return false;
                }
                y = y - 1;
                break;
                
            case 'S':
                if(!inMap(x,y+1)){
                    printUnableMove();
                    return false;
                }
                y = y + 1;
                break;
                
               
            case 'E':
                if(!inMap(x+1,y)){
                    printUnableMove();
                    return false;
                }
             x = x+1;
             break;
             
             
            case 'W':
                if(!inMap(x-1,y)){
                    printUnableMove();
                    return false;
                }
                x = x-1;
                break;
           
        }
        LastDirection = direction;
        if(isBomb()){
            System.out.println("Bomb Found!!!");
        }
        return true;
    }
    
    public boolean isBomb(){
        if(x==bx && y==by){
            return true;
        }
        return false;
    }
    
    public void printUnableMove(){
        System.out.println("I can't move. T_T ");
    }
    
    public String toString (){
        return "Robot X-Axis = "+this.x+" Y-Axis = "+this.y+"    |    "+" From bomb X-axis about = "+ Math.abs(this.bx-this.x)+" "+" From bomb Y-axis about = "+ Math.abs(this.y-this.by);
    }
    
    public boolean walk (char Direction , int step){
        for(int i =0;i<step;i++){
            if(!this.walk(Direction)){
                return false;
            }
            
        }return true;
    }
    public boolean walk (){
        return this.walk(LastDirection);
    }
    
    public boolean walk (int step){
        return this.walk(LastDirection , step);
    }
}